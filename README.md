# Trabajo de investigación: Conflicto y cambio social en la Argentina contemporánea
Trabajo de investigación y visualización de datos realizado para la Universidad de Buenos Aires. El trabajo, se propuso indagar sobre el proceso de conflictividad desarrollado por sectores obreros y estudiantiles entre los años 1966 y 1968 previo al denominado cordobazo.

Para la elaboración de esta visualización, se utilizó como principal insumo material periodístico de los diarios "La Nación", "Crónica" y "Clarín.

## Herramientas utilizadas
  + DC.JS
  + JQuery
  + D3.js
  + Leaflet
  + Heatmap
  
## Normalización de datos
  + OpenRefine 

## [Demo](https://franciscolaborda.github.io/conflictividad_en_los_setenta/.)
  
